﻿using System.Web.Mvc;
using BLL.HelpersAndUtils;
using BLL.SearchLogic;
using BLL.VKLogic;
using Unity;
using Unity.Lifetime;
using Unity.Mvc5;

namespace PandaWiki
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here 
            // it is NOT necessary to register your controllers 

            container.RegisterType<IAppConfiguration, AppConfiguration>(new HierarchicalLifetimeManager());
            container.RegisterType<IYoutubeSearch, YoutubeSearch>(new HierarchicalLifetimeManager());
            container.RegisterType<IVkService, VkService>(new HierarchicalLifetimeManager());

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}