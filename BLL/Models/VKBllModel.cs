﻿using System;

namespace BLL.Models
{
    /// <summary>
    /// Модель для VK
    /// </summary>
    public class VKBllModel
    {
        /// <summary>
        /// ID VK
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Юзер к которому прикреплено это видео
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// Ссылка на видео в VK
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// Юзер который выложил видео в VK
        /// </summary>
        public string User { get; set; }
        /// <summary>
        /// Количество коментариев
        /// </summary>
        public int? CommentCount { get; set; }
        /// <summary>
        /// Коментарии
        /// </summary>
        public string Comments { get; set; }
        /// <summary>
        /// Количество лайков
        /// </summary>
        public int Likes { get; set; }
    }
}
