﻿using InstaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.HelpersAndUtils
{
    public interface IInstagramConfig
    {
        InstagramConfig config { get; }
    }
}
