﻿using InstaSharp;
using System.Configuration;

namespace BLL.HelpersAndUtils
{
    public class InstagramConfigs : IInstagramConfig
    {
        public InstagramConfig config { get; }

        public InstagramConfigs()
        {
            var clientId = ConfigurationManager.AppSettings["client_id"];
            var clientSecret = ConfigurationManager.AppSettings["client_secret"];
            var redirectUri = ConfigurationManager.AppSettings["redirect_uri"];
            var realtimeUri = "";

            config = new InstagramConfig(clientId, clientSecret, redirectUri, realtimeUri);
        }
    }
}
