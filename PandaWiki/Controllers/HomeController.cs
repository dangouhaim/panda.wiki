﻿using BLL.SearchLogic;
using BLL.VKLogic;
using PandaWiki.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PandaWiki.Controllers
{
    public class HomeController : Controller
    {
        private readonly IVkService _vkService;
        private readonly IYoutubeSearch _youtubeService;

        public HomeController (IVkService vkService, IYoutubeSearch youtubeService)
        {
            _youtubeService = youtubeService;
            _vkService = vkService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public RedirectResult RedirectToResultPage(string q)
        {
            return RedirectPermanent("/Search/Index?q=" + HttpUtility.UrlEncode(q));
        }

        /// <summary>
        /// Поиск видео
        /// </summary>
        /// <param name="search"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public ActionResult SearchVideo(string search, List<Messangers> result)
        {

            if(result == null)
                return View("Index");

            foreach (var t in result)
            {
                switch (t)
                {
                    case Messangers.YouTube:
                        {
                            _youtubeService.GetYoutubeResult(search);
                            break;
                        }
                    case Messangers.Vk:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Facebook:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Instagram:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Pinterest:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Twitter:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Vikipedia:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                }
            }
            


            return null;
        }

        /// <summary>
        /// Поиск Картинок
        /// </summary>
        /// <param name="search"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public ActionResult SearchPictiries(string search, List<Messangers> result)
        {
            foreach (var t in result)
            {
                switch (t)
                {
                    case Messangers.Vk:
                        {
                            _vkService.GetPhotosFromVk(search);
                            break;
                        }
                    case Messangers.Facebook:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Instagram:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Pinterest:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Twitter:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                }
            }



            return null;
        }

        /// <summary>
        /// Поиск Постов
        /// </summary>
        /// <param name="search"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public ActionResult SearchPosts(string search, List<Messangers> result)
        {
            foreach (var t in result)
            {
                switch (t)
                {
                    case Messangers.Vk:
                        {
                            _vkService.GetPostsFromVk(search);
                            break;
                        }
                    case Messangers.Facebook:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Instagram:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Pinterest:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Twitter:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                }
            }



            return null;
        }

        /// <summary>
        /// Поиск пользователей
        /// </summary>
        /// <param name="search"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public ActionResult SearchPeoples(string search, List<Messangers> result)
        {
            foreach (var t in result)
            {
                switch (t)
                {
                    case Messangers.Vk:
                        {
                            _vkService.GetPeoplesFromVk(search);
                            break;
                        }
                    case Messangers.Facebook:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Instagram:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Pinterest:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Twitter:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                }
            }



            return null;
        }

        /// <summary>
        /// Поиск музыки
        /// </summary>
        /// <param name="search"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public ActionResult SearchMusic(string search, List<Messangers> result)
        {
            foreach (var t in result)
            {
                switch (t)
                {
                    case Messangers.YouTube:
                        {
                            _vkService.GetPeoplesFromVk(search);
                            break;
                        }
                    case Messangers.Vk:
                        {
                            _vkService.GetMusicFromVk(search);
                            break;
                        }
                    case Messangers.Facebook:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Instagram:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Pinterest:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                    case Messangers.Twitter:
                        {
                            _vkService.GetVkInfoFromSerch(search);
                            break;
                        }
                }
            }



            return null;
        }
    }
}