﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ResponseModel
{
    public class SearchModelResponse
    {
        public string Type { get; set; }
        public string Description { get; set; }
        public string UrlThumbnail { get; set; }
        public string YoutubeVideoId { get; set; }
        public string Title { get; set; }
    }
}
