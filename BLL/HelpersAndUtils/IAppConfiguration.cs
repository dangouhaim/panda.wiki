﻿namespace BLL.HelpersAndUtils
{
    public interface IAppConfiguration
    {
        string YouTubeApiKey { get; }
    }
}
