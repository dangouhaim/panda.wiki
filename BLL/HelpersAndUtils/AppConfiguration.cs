﻿using System.Configuration;

namespace BLL.HelpersAndUtils
{
    public class AppConfiguration : IAppConfiguration
    {
        public AppConfiguration()
        {
            YouTubeApiKey = ConfigurationManager.AppSettings["YotubeApiKey"];
        }

        public string YouTubeApiKey { get; }
    }
}
