﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.ResponseModel;

namespace BLL.SearchLogic
{
    public interface IYoutubeSearch
    {
        Task<IEnumerable<SearchModelResponse>> GetYoutubeResult(string q, int maxResults = 50);
    }
}
