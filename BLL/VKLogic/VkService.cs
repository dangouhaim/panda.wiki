﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Model;
using VkNet.Model.RequestParams;
using VkNet.Utils;

namespace BLL.VKLogic
{
    /// <summary>
    /// Реализация сервеса по работе с VK
    /// </summary>
    public class VkService : IVkService
    {
        private readonly VkApi _api;

        public VkService()
        {
            _api = new VkApi();
            var _param = new ApiAuthParams
            {
                ApplicationId = Convert.ToUInt32(ConfigurationManager.AppSettings["ApplicationId"]),
                Login = ConfigurationManager.AppSettings["LoginForVk"],
                Password = ConfigurationManager.AppSettings["PasswordForVk"],
                Settings = Settings.All
            };
            _api.Authorize(_param);
        }
        /// <summary>
        /// Добавить видео с VK пользователю
        /// </summary>
        /// <param name="vkModel"></param>
        public void AddVkForUser(Guid UserId, VKBllModel vkModel)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Получить все видео у определенного юзера
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VKBllModel> GetAllVkUser(Guid UserId)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Поиск в VK по ключевому слову
        /// </summary>
        /// <param name="search">Ключевое слово</param>
        /// <returns></returns>
        public IEnumerable<VKBllModel> GetVkInfoFromSerch(string search)
        {
            try
            {
                var vkResult = new List<VKBllModel>();
                
                var result = _api.Video.Search(new VideoSearchParams { Query = search });
                foreach (var t in result)
                {
                    var h = _api.Users.Get(new List<long> { (long)t.OwnerId })?.FirstOrDefault();
                    var item = new VKBllModel
                    {
                        Id = Guid.NewGuid(),
                        Url = t.Player.AbsoluteUri,
                        Likes = t.Likes.Count,
                        CommentCount = t.Comments,
                        User = h.FirstName
                    };

                    vkResult.Add(item);
                }

                return vkResult;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public IEnumerable<VKBllModel> GetPhotosFromVk(string search)
        {
            try
            {
                var vkResult = new List<VKBllModel>();
                var result = _api.Photo.Search(new PhotoSearchParams { Query = search });

                foreach (var t in result)
                {
                    var h = _api.Users.Get(new List<long> { (long)t.OwnerId})?.FirstOrDefault();
                    var item = new VKBllModel
                    {
                        Id = Guid.NewGuid(),
                        Url = t.Photo1280.AbsoluteUri,
                        Likes = t.Likes.Count,
                        Comments = t.Comments.ToString(),
                        User = h.FirstName
                    };

                    vkResult.Add(item);
                }

                return vkResult;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Получить все посты в зависимости от поиска
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public IEnumerable<VKBllModel> GetPostsFromVk(string search)
        {
            try
            {
                var vkResult = new List<VKBllModel>();
                var result = _api.Wall.Search(new WallSearchParams { Query = search }).WallPosts;

                foreach (var t in result)
                {
                    var h = _api.Users.Get(new List<long> { (long)t.OwnerId })?.FirstOrDefault();
                    var item = new VKBllModel
                    {
                        Id = Guid.NewGuid(),
                        Comments = t.Text,
                        User = h.FirstName
                    };

                    vkResult.Add(item);
                }

                return vkResult;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Получить всех пользователей в зависимости от поиска
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public IEnumerable<VKBllModel> GetPeoplesFromVk(string search)
        {
            try
            {
                var vkResult = new List<VKBllModel>();
                var result = _api.Users.Get(new List<string> { search });

                foreach (var t in result)
                {
                    var item = new VKBllModel
                    {
                        Id = Guid.NewGuid(),
                        User = t.FirstName
                    };

                    vkResult.Add(item);
                }

                return vkResult;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Получить композиции в зависимости от поиска
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public IEnumerable<VKBllModel> GetMusicFromVk(string search)
        {
            try
            {
                var vkResult = new List<VKBllModel>();
                var result = _api.Wall.Search(new WallSearchParams { Query = search }).WallPosts;

                foreach (var t in result)
                {
                    var h = _api.Users.Get(new List<long> { (long)t.OwnerId })?.FirstOrDefault();
                    var item = new VKBllModel
                    {
                        Id = Guid.NewGuid(),
                        Comments = t.Text,
                        User = h.FirstName
                    };

                    vkResult.Add(item);
                }

                return vkResult;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Удаление видео у определенного пользователя
        /// </summary>
        /// <param name="id"></param>
        public void RemoveVkFromUser(Guid VkId, Guid UserID)
        {
            try
            {

            }
            catch (Exception e)
            {

            }
        }
    }
}
