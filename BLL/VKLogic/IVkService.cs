﻿using BLL.Models;
using System;
using System.Collections.Generic;

namespace BLL.VKLogic
{
    /// <summary>
    /// Сервис по работе с VK
    /// </summary>
    public interface IVkService
    {
        /// <summary>
        /// Поиск в VK по ключевому слову
        /// </summary>
        /// <param name="search">Ключевое слово</param>
        /// <returns></returns>
        IEnumerable<VKBllModel> GetVkInfoFromSerch(string search);
        /// <summary>
        /// Получить все видео у определенного юзера
        /// </summary>
        /// <returns></returns>
        IEnumerable<VKBllModel> GetAllVkUser(Guid UserId);
        /// <summary>
        /// Добавить видео с VK пользователю
        /// </summary>
        /// <param name="vkModel"></param>
        void AddVkForUser(Guid UserId, VKBllModel vkModel);
        /// <summary>
        /// Удаление видео у определенного пользователя
        /// </summary>
        /// <param name="id"></param>
        void RemoveVkFromUser(Guid VkId, Guid UserID);

        /// <summary>
        /// Поиск картинов в ВК
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        IEnumerable<VKBllModel> GetPhotosFromVk(string search);

        /// <summary>
        /// Поиск постов в ВК
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        IEnumerable<VKBllModel> GetPostsFromVk(string search);

        /// <summary>
        /// Получить композиции в зависимости от поиска
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        IEnumerable<VKBllModel> GetMusicFromVk(string search);

        /// <summary>
        /// Поиск пользователей в ВК
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        IEnumerable<VKBllModel> GetPeoplesFromVk(string search);
    }
}
