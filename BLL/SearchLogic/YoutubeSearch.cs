﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.HelpersAndUtils;
using DAL.Helpers;
using DAL.ResponseModel;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;

namespace BLL.SearchLogic
{
    public class YoutubeSearch : IYoutubeSearch
    {
        private readonly IAppConfiguration _config;

        public YoutubeSearch(IAppConfiguration config)
        {
            _config = config;
        }

        public async Task<IEnumerable<SearchModelResponse>> GetYoutubeResult(string q, int maxResults = 50)
        {
            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                ApiKey = _config.YouTubeApiKey,
                ApplicationName = GetType().ToString()
            });

            var searchListRequest = youtubeService.Search.List("snippet");
            searchListRequest.Q = q; // Replace with your search term.
            searchListRequest.MaxResults = maxResults;

            // Call the search.list method to retrieve results matching the specified query term.
            var searchListResponse = await searchListRequest.ExecuteAsync();

            // Add each result to the appropriate list, and then display the lists of
            // matching videos, channels, and playlists.

            var searchModelResponses = searchListResponse.Items.Select(x => new SearchModelResponse
            {
                Type = SearchTypes.YouTube,
                Description = x.Snippet?.Description,
                UrlThumbnail = x.Snippet?.Thumbnails?.Default__?.Url,
                YoutubeVideoId = x.Id.VideoId,
                Title = x.Snippet?.Title
            });

            return searchModelResponses;
        }
    }
}
