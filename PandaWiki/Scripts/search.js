﻿"Use strict";

var search = function(q) {
    if (!q) return;
    $.ajax({
        dataType: "html",
        url: "/Search/YouTube",
        data: {
            q: q.trim(),
            maxResults: 50
        },
        success: function (response) {
            console.log(response);
            $("#loader").hide();
            $("#posts-containers").html(response);
        },
        error: function (err) {
            console.log("error: ");
            console.log(err);
        } 
    });

}

$(document).ready(function() {
    const q = $("#dropdownMenu-search").val();
    search(q);
});