﻿using System.Threading.Tasks;
using System.Web.Mvc;
using BLL.SearchLogic;
using DAL.RequestModel;

namespace PandaWiki.Controllers
{
    public class SearchController : Controller
    {
        private readonly IYoutubeSearch _youtubeSearch;

        public SearchController(IYoutubeSearch youtubeSearch)
        {
            _youtubeSearch = youtubeSearch;
        }

        // GET: Search
        public ActionResult Index([Bind(Include = "q")] QuerySearchModel query)
        {
            return View("Index", query);
        }

        public async Task<ActionResult> YouTube(string q, int maxResults)
        {
            var res = await _youtubeSearch.GetYoutubeResult(q, maxResults);
            return PartialView("Results", res);
        }
    }
}