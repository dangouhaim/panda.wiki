﻿using BLL.VKLogic;
using System.Web.Mvc;

namespace PandaWiki.Controllers
{
    /// <summary>
    /// Контроллер по работе с VK Api
    /// </summary>
    public class VKController : Controller
    {
        private readonly IVkService _vkService;

        public VKController(IVkService vkService)
        {
            _vkService = vkService;
        }

        /// <summary>
        /// Поиск видео в ВК
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public ActionResult SearchVideoToVk(string search)
        {
            var items = _vkService.GetVkInfoFromSerch(search);

            return View();
        }

        /// <summary>
        /// Поиск картинок в ВК
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public ActionResult SearchPhotosToVk(string search)
        {
            var items = _vkService.GetPhotosFromVk(search);

            return View();
        }
    }
}